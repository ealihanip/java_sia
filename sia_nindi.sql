-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2018 at 09:24 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sia_nindi`
--

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `no_referensi` varchar(10) NOT NULL,
  `tgl_trans` date NOT NULL,
  `no_bukti_trans` varchar(10) NOT NULL,
  `transaksi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_detail`
--

CREATE TABLE `jurnal_detail` (
  `no_referensi` varchar(10) NOT NULL,
  `Kode_Akun` varchar(5) NOT NULL,
  `debet` double NOT NULL,
  `kredit` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kas_in`
--

CREATE TABLE `kas_in` (
  `no_km` varchar(10) NOT NULL,
  `tgl_km` date NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas_in`
--

INSERT INTO `kas_in` (`no_km`, `tgl_km`, `keterangan`) VALUES
('KM00000001', '2018-05-10', 'asdasd'),
('KM00000003', '2018-05-04', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `kas_in_detail`
--

CREATE TABLE `kas_in_detail` (
  `no_km` varchar(10) NOT NULL,
  `kode_akun` varchar(5) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas_in_detail`
--

INSERT INTO `kas_in_detail` (`no_km`, `kode_akun`, `nominal`) VALUES
('KM00000001', '11101', 200000),
('KM00000001', '11101', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `kas_out`
--

CREATE TABLE `kas_out` (
  `no_kk` varchar(10) NOT NULL,
  `tgl_kk` date NOT NULL,
  `memo` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas_out`
--

INSERT INTO `kas_out` (`no_kk`, `tgl_kk`, `memo`) VALUES
('KK00000001', '2018-05-02', 'asd'),
('KK00000002', '2018-05-03', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `kas_out_detail`
--

CREATE TABLE `kas_out_detail` (
  `no_kk` varchar(10) NOT NULL,
  `kode_akun` varchar(5) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas_out_detail`
--

INSERT INTO `kas_out_detail` (`no_kk`, `kode_akun`, `nominal`) VALUES
('KK00000001', '11101', 20000),
('KK00000001', '11101', 20000),
('KK00000002', '11104', 200000),
('KK00000002', '11104', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun`
--

CREATE TABLE `master_akun` (
  `kode_akun` varchar(5) NOT NULL,
  `nama_akun` varchar(50) NOT NULL,
  `jenis_akun` varchar(25) NOT NULL,
  `saldo_normal` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun`
--

INSERT INTO `master_akun` (`kode_akun`, `nama_akun`, `jenis_akun`, `saldo_normal`) VALUES
('11101', 'Kas', 'Kas/Bank', 'Debet'),
('11102', 'test', 'Kas/Bank', 'Debet');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `thn` int(4) NOT NULL,
  `Awal_Bulan` varchar(15) NOT NULL,
  `Akhir_Bulan` varchar(15) NOT NULL,
  `Status` enum('Aktif','Nonaktif') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`thn`, `Awal_Bulan`, `Akhir_Bulan`, `Status`) VALUES
(2016, 'Januari', 'Desember', 'Nonaktif'),
(2017, 'Januari', 'Desember', 'Nonaktif');

-- --------------------------------------------------------

--
-- Table structure for table `saldo_awal`
--

CREATE TABLE `saldo_awal` (
  `kode_akun` varchar(5) NOT NULL,
  `saldo_debet` double NOT NULL,
  `saldo_kredit` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_Name` varchar(10) NOT NULL,
  `Nama_User` varchar(25) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `Hak_Akses` varchar(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_Name`, `Nama_User`, `Password`, `Hak_Akses`) VALUES
('K01', 'Rifky Permana', '1992-01-16', 'Admin'),
('K02', 'Nia Kurniasih', '1990-03-15', 'Admin'),
('K03', 'Shakila Mazaya Azzahra', '2016-11-29', 'User'),
('K04', 'Masaya Zati Aqmarina', '1997-12-31', 'User'),
('K05', 'Meizsa Krismalinda', '1996-05-05', 'User'),
('K06', 'Melody Nurramdhani', '1992-03-24', 'Admin'),
('K07', 'Adhisty Zara', '2003-06-21', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd`
--

CREATE TABLE `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`no_referensi`);

--
-- Indexes for table `kas_in`
--
ALTER TABLE `kas_in`
  ADD PRIMARY KEY (`no_km`);

--
-- Indexes for table `kas_out`
--
ALTER TABLE `kas_out`
  ADD PRIMARY KEY (`no_kk`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`thn`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_Name`);

--
-- Indexes for table `user_pwd`
--
ALTER TABLE `user_pwd`
  ADD PRIMARY KEY (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
