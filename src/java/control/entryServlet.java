/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Entry;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author hanep
 */
@WebServlet(name = "entryServlet", urlPatterns = {"/entryServlet"})
public class entryServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException,
            SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");

        Entry entry = new Entry();  //--Inisialisasi Object—

        String URL = "jdbc:mysql://localhost:3306/sia_nindi";
        String USERNAME = "root";
        String PASSWORD = "";
        Statement stmt = null;
        ResultSet rs = null;
        Connection koneksi = null;
        PreparedStatement pstmt = null;
        int result = 0;

        try (PrintWriter out = response.getWriter()) {
            Class.forName("com.mysql.jdbc.Driver");
            koneksi = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            String aksi = request.getParameter("aksi");

            stmt = koneksi.createStatement();
            String kode = request.getParameter("kode");
            rs = stmt.executeQuery("SELECT COUNT(kode_akun) AS jumlah FROM saldo_awal"
                    + " WHERE kode_akun = '" + kode + "'");
            if (rs.next()) {

                if (rs.getString("jumlah").equalsIgnoreCase("1")) {

                    aksi = "Update";
                }
            }

            if (aksi != null) {
                entry.setKode_akun(request.getParameter("kode"));
                entry.setSaldo_normal(request.getParameter("saldo_normal"));

                if (entry.getSaldo_normal().equalsIgnoreCase("Debet")) {
                    entry.setSaldo_debet(request.getParameter("saldo"));
                    entry.setSaldo_kredit("0");
                } else {

                    entry.setSaldo_kredit(request.getParameter("saldo"));
                    entry.setSaldo_debet("0");

                }

                switch (aksi) {

                    case "Simpan":
                        pstmt = koneksi.prepareStatement("INSERT INTO saldo_awal"
                                + " VALUES(?, ?, ?)");
                        pstmt.setString(1, entry.getKode_akun());
                        pstmt.setString(2, entry.getSaldo_debet());
                        pstmt.setString(3, entry.getSaldo_kredit());

                        result = pstmt.executeUpdate();
                        if (result > 0) {
                            out.println("<script>alert('Kas Masuk Berhasil Disimpan...!!!')</script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_saldo_awal'/>");
                        } else {

                            out.println("<script>alert('Kas Masuk Berhasil Disimpan...!!!')</script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_saldo_awal'/>");
                        }
                        break;
                    case "Update":
                        pstmt = koneksi.prepareStatement("UPDATE saldo_awal SET"
                                + " saldo_debet = ?,"
                                + " saldo_kredit = ?"
                                + " WHERE kode_akun = ?");

                        pstmt.setString(1, entry.getSaldo_debet());
                        pstmt.setString(2, entry.getSaldo_kredit());
                        pstmt.setString(3, entry.getKode_akun());
                        result = pstmt.executeUpdate();
                        if (result > 0) {
                            out.println("<script>alert('Kas Masuk Berhasil Disimpan...!!!')</script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_saldo_awal'/>");
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
