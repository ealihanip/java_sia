/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.mysql.jdbc.PreparedStatement;
import model.periode;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "PeriodeServlet", urlPatterns = {"/PeriodeServlet"})
public class PeriodeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        periode periode = new periode();
        String URL = "jdbc:mysql://localhost:3306/sia_nindi";
        String USERNAME = "root";
        String PASSWORD = "";
        Connection koneksi = null;
        PreparedStatement pstmt = null;
        int result = 0;
        try (PrintWriter out = response.getWriter()) {
            Class.forName("com.mysql.jdbc.Driver");
            koneksi = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            String aksi = request.getParameter("aksi");
            if (aksi != null) {
                periode.setThn(request.getParameter("tahun"));
                periode.setAwal_bulan(request.getParameter("awal_bulan"));
                periode.setAkhir_bln(request.getParameter("akhir_bulan"));
                periode.setStatus(request.getParameter("status"));
                switch (aksi) {
                    case "Insert":

                        koneksi.createStatement().execute("insert into periode "
                                + "values('" + periode.getThn() + "','" + periode.getAwal_bulan() + "','" + periode.getAkhir_bln() + "','" + periode.getStatus() + "')");
                        out.println("<script>alert('Data Periode berhasil Disimpan...!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_periode'/>");
                        break;
                    case "Update":

                        koneksi.createStatement().execute("update periode set "
                                + "Awal_Bulan='" + periode.getAwal_bulan() + "', "
                                + "Akhir_Bulan='" + periode.getAkhir_bln() + "', "
                                + "Status='" + periode.getStatus() + "' "
                                + "where thn='" + periode.getThn() + "'");
                        //out.println(periode.getThn());
                        out.println("<script>alert('Data Periode berhasil Diubah...!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_periode'/>");
                        break;
                    case "Delete":
                        pstmt = (PreparedStatement) koneksi.prepareStatement("DELETE FROM "
                                + "periode WHERE thn = ?");
                        pstmt.setString(1, periode.getThn());
                        result = pstmt.executeUpdate();
                        if (result >= 0) {
                            out.print("<script language=JavaScript>"
                                    + "alert ('Data Berhasil Dihapus');"
                                    + "document.location='index.jsp?halaman=data_periode';"
                                    + "</script>");
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PeriodeServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PeriodeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PeriodeServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PeriodeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
