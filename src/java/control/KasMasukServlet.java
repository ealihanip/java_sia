/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author se7enSky
 */
@WebServlet(name = "KasMasukServlet", urlPatterns = {"/KasMasukServlet"})
public class KasMasukServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            /* TODO output your page here. You may use following sample code. */
            String aksi = request.getParameter("aksi");
            String no_akunkredit = request.getParameter("no_akunkredit");
            String no_km = request.getParameter("no_km");
            String keterangan = request.getParameter("keterangan");
            String nominal = request.getParameter("nominal");
            String tgl_km = request.getParameter("tgl_km");
            String no_akundebet = request.getParameter("no_akundebet");
            String cari = request.getParameter("cari");
            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/sia_nindi", "root", "");
            
            
            switch (aksi) {

                case "SIMPAN":
                    if (no_akundebet.equals("") || no_akunkredit.equals("") || keterangan.equals("") || 
                        no_km.equals("") || no_akundebet.equals("") || tgl_km.equals("")) {
                        out.println("<script>alert('Gagal... masih ada data yang belum terisi, Silahkan Ulangi!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_kas_masuk'/>");
                    } else {
                        koneksi.createStatement().execute("insert into kas_in(no_km,tgl_km,keterangan) "
                                + "values('" + no_km + "','" + tgl_km + "','" + keterangan + "')");
                        for (int i = 1; i <= 2; i++) {
                            if (i % 2 == 0) {
                                koneksi.createStatement().execute("insert into kas_in_detail(no_km,kode_akun,nominal)"
                                        + "values('" + no_km + "','" + no_akunkredit + "','" + nominal + "')");
                            } else if (i % 2 == 1) {
                                koneksi.createStatement().execute("insert into kas_in_detail(no_km,kode_akun,nominal) "
                                        + "Values('" + no_km + "','" + no_akundebet + "','" + nominal + "')");
                            }
                        }
                        out.println("<script>alert('Kas Masuk Berhasil Disimpan...!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_kas_masuk'/>");
                    }
                    break;

                case "HAPUS":
                    koneksi.createStatement().execute("delete from kas_in where no_km='" + no_km + "' ");
                    koneksi.createStatement().execute("delete from kas_in_detail where no_km='" + no_km + "' ");
                    koneksi.createStatement().execute("delete from kas_in where no_km='" + no_km + "' ");

                    out.println("<script>alert('Kas Masuk berhasil Dihapus...!!!')</script>"
                            + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_kas_masuk'/>");
                    break;

                default:
                    break;
            }

        } catch (ClassNotFoundException | SQLException e) {
            out.println(e);
            out.println("<script>alert('Gagal... Diproses, Silahkan Ulangi!!!!!!')</script>"
                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_kas_masuk'/>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
