/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author se7enSky
 */
@WebServlet(name = "JurnalServlet", urlPatterns = {"/JurnalServlet"})
public class JurnalServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/sia_nindi", "root", "");

            String aksi = request.getParameter("aksi");

            String no_referensi = request.getParameter("no_referensi");
            String tgl_trans = request.getParameter("tgl_trans");
            String no_bukti_transaksi = request.getParameter("no_bukti_transaksi");
            String keterangan = request.getParameter("keterangan");

            String[] pilih = request.getParameterValues("pilih[]");
            String[] no_akunkredit=request.getParameterValues("no_akunkredit");
            String[] debet = request.getParameterValues("debet");
            String[] kredit = request.getParameterValues("kredit");

            String bal = request.getParameter("Balance");

            if (bal.equals("0")) {

                
            }

            switch (aksi) {

                case "simpan":

                    if (no_referensi.equals("") || tgl_trans.equals("") || no_bukti_transaksi.equals("")
                            || keterangan.equals("")) {

                        out.println("<script>alert('Gagal... masih ada data yang belum terisi, Silahkan Ulangi!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=jurnal_umum'/>");
                    } else if (pilih == null) {
                        out.println("<script>alert('silahkan pilih data dulu')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=jurnal_umum'/>");
                    } else {

                        koneksi.createStatement().execute("insert into jurnal "
                                + "values('" + no_referensi + "','" + tgl_trans + "','" + no_bukti_transaksi + "','" + keterangan + "')");
                        
                        
                        for (int i = 0; i < pilih.length; i++) {

                            int pilihact = Integer.parseInt(pilih[i]);

                            pilihact = pilihact - 1;

                            
                            
                            koneksi.createStatement().execute("insert into jurnal_detail "
                                + "values('" + no_referensi + "','" + no_akunkredit[pilihact] + "','" + debet[pilihact] + "','" + kredit[pilihact] + "')");
                        

                        }
                        out.println("<script>alert('Data Jurnal Umum Berhasil Disimpan...!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=jurnal_umum'/>");
                    }

                    break;
                default:
                    break;
            }

        } catch (ClassNotFoundException | SQLException e) {
            out.println(e);
            out.println("<script>alert('Gagal... Diproses, Silahkan Ulangi!!!!!!')</script>"
                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=jurnal_umum'/>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
