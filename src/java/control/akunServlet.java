package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Akun;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;


@WebServlet(name = "akunServlet", urlPatterns = {"/akunServlet"})
public class akunServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException,
            SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");

        Akun akun = new Akun();  //--Inisialisasi Object—

        String URL = "jdbc:mysql://localhost:3306/sia_nindi";
        String USERNAME = "root";
        String PASSWORD = "";

        Connection koneksi = null;
        PreparedStatement pstmt = null;
        int result = 0;

        try (PrintWriter out = response.getWriter()) {
            Class.forName("com.mysql.jdbc.Driver");
            koneksi = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            String aksi = request.getParameter("aksi");
            
            
            
            
            if (aksi != null) {
                akun.setKode_akun(request.getParameter("kode"));
                akun.setNama_akun(request.getParameter("nama"));
                akun.setJenis_akun(request.getParameter("jenis"));
                akun.setSaldo_normal(request.getParameter("saldo"));

                switch (aksi) {
                    case "Insert":
                        pstmt = koneksi.prepareStatement("INSERT INTO master_akun"
                                + " VALUES(?, ?, ?, ?)");
                        pstmt.setString(1, akun.getKode_akun());
                        pstmt.setString(2, akun.getNama_akun());
                        pstmt.setString(3, akun.getJenis_akun());
                        pstmt.setString(4, akun.getSaldo_normal());

                        result = pstmt.executeUpdate();
                        if (result > 0) {
                            out.println("<script> "
                                    + "alert('Data telah ditambahkan');"
                                    + " </script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_akun'/>';"
                                    );
                        }
                        break;
                    //--code yang ditambahkan-- 
                    case "Update":
                        pstmt = koneksi.prepareStatement("UPDATE master_akun SET"
                                + " nama_akun = ?,"
                                + " jenis_akun = ?,"
                                + " saldo_normal = ?"
                                + " WHERE kode_akun = ?");

                        pstmt.setString(1, akun.getNama_akun());
                        pstmt.setString(2, akun.getJenis_akun());
                        pstmt.setString(3, akun.getSaldo_normal());
                        pstmt.setString(4, akun.getKode_akun());

                        result = pstmt.executeUpdate();
                        if (result > 0) {
                            out.println("<script> " + "alert('Data telah di Update');"
                                    + " </script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_akun'/>';"
                                    );
                        }
                        break;
                    //--end update--
                    //--code yang ditambahkan-- 
                    case "Delete":
                        pstmt = koneksi.prepareStatement("DELETE FROM"
                                + " master_akun WHERE kode_akun = ?");
                        pstmt.setString(1, akun.getKode_akun());
                        result = pstmt.executeUpdate();

                        if (result > 0) {
                            out.println("<script> "
                                    + "alert('Data telah di Hapus');"
                                    + " </script>"
                                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_akun'/>';"
                                    );
                        }
                        break;
                    //--end delete--
                    default:
                        break;
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(akunServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
