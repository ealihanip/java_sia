/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author se7enSky
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/UserServlet"})
public class UserServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            String aksi = request.getParameter("aksi");
            String uname = request.getParameter("uname");
            String nm_admin = request.getParameter("nm_admin");
            String pswd = request.getParameter("pswd");
            String hak = request.getParameter("hak");
            
            
            Class.forName("com.mysql.jdbc.Driver");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/sia_nindi", "root", "");
            switch (aksi) {
                case "TAMBAH":
                    if (uname.equals("") || nm_admin.equals("") || pswd.equals("") || hak.equals("")) {
                        out.println("<script>alert('Gagal... masih ada data yang belum terisi, Silahkan Ulangi!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_user'/>");
                    } else {
                        koneksi.createStatement().execute("insert into user "
                                + "values('" + uname + "','" + nm_admin + "','" + pswd + "','" + hak + "')");
                        out.println("<script>alert('Data Master Admin berhasil Disimpan...!!!')</script>"
                                + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_user'/>");
                    }
                    break;
                case "UBAH":
                    koneksi.createStatement().execute("update user set "
                            + "Nama_user='" + nm_admin + "', Password='" + pswd + "', Hak_Akses='" + hak + "' "
                            + "where User_Name='" + uname + "'");
                    out.println("<script>alert('Data Master Admin berhasil Diubah...!!!')</script>"
                            + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_user'/>");
                    break;
                case "HAPUS":
                    koneksi.createStatement().execute("delete from user where User_Name='" + uname + "'");
                    out.println("<script>alert('Data Master Admin berhasil Dihapus...!!!')</script>"
                            + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_user'/>");
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            out.println("<script>alert('Gagal... Kemungkinan Username sudah ada, Silahkan Ulangi!!!')</script>"
                    + "<meta http-equiv='refresh' content='1;index.jsp?halaman=data_user'/>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
