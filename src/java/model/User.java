package model;

public class User{

    private String uname;
    private String namalengkap;
    private String password;
    private String hak;

    public String getuname() {
        return uname;
    }

    public void setuname(String uname) {
        this.uname = uname;
    }

    public String getnamalengkap() {
        return namalengkap;
    }

    public void setnamalengkap(String namalengkap) {
        this.namalengkap = namalengkap;
    }

    public String getpassword() {
        return password;
    }

    public void setpassword(String password) {
        this.password = password;
    }

    public String gethak() {
        return hak;
    }

    public void sethak(String hak) {
        this.hak = hak;
    }

}
