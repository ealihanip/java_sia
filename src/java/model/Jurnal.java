/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nindi Musliyanti
 */
public class Jurnal {
    private String no_referensi;
    private String tgl_trans;
    private String no_bukti_trans;
    private String keterangan;
    private double debet;
    private double kredit;

    public String getNo_referensi() {
        return no_referensi;
    }

    public void setNo_referensi(String no_referensi) {
        this.no_referensi = no_referensi;
    }

    public String getTgl_trans() {
        return tgl_trans;
    }

    public void setTgl_trans(String tgl_trans) {
        this.tgl_trans = tgl_trans;
    }

    public String getNo_bukti_trans() {
        return no_bukti_trans;
    }

    public void setNo_bukti_trans(String no_bukti_trans) {
        this.no_bukti_trans = no_bukti_trans;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public double getDebet() {
        return debet;
    }

    public void setDebet(double debet) {
        this.debet = debet;
    }

    public double getKredit() {
        return kredit;
    }

    public void setKredit(double kredit) {
        this.kredit = kredit;
    }
    
}
