/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author hanep
 */
public class Entry {

    private String kode_akun;
    private String nama_akun;
    private String jenis_akun;
    private String saldo_normal;
    private String saldo_debet;
    private String saldo_kredit;

    public String getKode_akun() {
        return kode_akun;
    }

    public void setKode_akun(String kode_akun) {
        this.kode_akun = kode_akun;
    }

    public String getNama_akun() {
        return nama_akun;
    }

    public void setNama_akun(String nama_akun) {
        this.nama_akun = nama_akun;
    }

    public String getJenis_akun() {
        return jenis_akun;
    }

    public void setJenis_akun(String jenis_akun) {
        this.jenis_akun = jenis_akun;
    }

    public String getSaldo_normal() {
        return saldo_normal;
    }

    public void setSaldo_normal(String saldo_normal) {
        this.saldo_normal = saldo_normal;
    }

    public String getSaldo_debet() {
        return saldo_debet;
    }

    public void setSaldo_debet(String saldo_debet) {
        this.saldo_debet = saldo_debet;
    }

    public String getSaldo_kredit() {
        return saldo_kredit;
    }

    public void setSaldo_kredit(String saldo_kredit) {
        this.saldo_kredit = saldo_kredit;
    }

    @Override
    public String toString() {
        return "Master Akun: [Kode Akun = " + kode_akun
                + ", Nama Akun = " + nama_akun
                + ", Jenis Akun = " + jenis_akun
                + ", Saldo_debet = " + saldo_debet
                + ", Saldo Normal = " + saldo_normal
                + ", Saldo_kredit = " + saldo_kredit + "]";
    }
}
