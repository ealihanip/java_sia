/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Fajriyah Endah kurnia Ningsih
 */
public class KasMasuk{
    private String no_km;
    private String tgl_km;
    private String keterangan;
    private String no_kredit;
    private String no_debet;
    private double nominal;

    public String getNo_km() {
        return no_km;
    }

    public void setNo_km(String no_km) {
        this.no_km = no_km;
    }

    public String getTgl_km() {
        return tgl_km;
    }

    public void setTgl_km(String tgl_km) {
        this.tgl_km = tgl_km;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNo_kredit() {
        return no_kredit;
    }

    public void setNo_kredit(String no_kredit) {
        this.no_kredit = no_kredit;
    }

    public String getNo_debet() {
        return no_debet;
    }

    public void setNo_debet(String no_debet) {
        this.no_debet = no_debet;
    }

    public double getNominal() {
        return nominal;
    }

    public void setNominal(double nominal) {
        this.nominal = nominal;
    }

    
}
