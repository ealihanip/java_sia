/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Fajriyah Endah Kurnia Ningsih
 */
public class periode {
    private String thn;
    private String awal_bulan;
    private String akhir_bln;
    private String status;

    public String getThn() {
        return thn;
    }

    public void setThn(String thn) {
        this.thn = thn;
    }

    public String getAwal_bulan() {
        return awal_bulan;
    }

    public void setAwal_bulan(String awal_bulan) {
        this.awal_bulan = awal_bulan;
    }

    public String getAkhir_bln() {
        return akhir_bln;
    }

    public void setAkhir_bln(String akhir_bln) {
        this.akhir_bln = akhir_bln;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @Override
    public String toString() {
        return "Periode: [Tahun = " + thn +
                ", Awal Bulan = " + awal_bulan +
                ", Akhir Bulan = " + akhir_bln +
                ", Status = " + status + "]";
        
                
    }

    public void setAkhir_bulan(String parameter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
