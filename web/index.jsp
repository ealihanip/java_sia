<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">


        <link rel="stylesheet" href="style.css" >

        <title>Nindi Musliyanti</title>
    </head>

    <body>

        <div class="row">
            <div class="col-sm-10">
                <div class="container">
                    <h2>Sistem Informasi Akuntansi</h2>
                    <h3>Bina Sarana Informatika</h3>
                    <h4>Jl. Ir. Haji Juanda No.17 Sarimulya, Kotabaru, Kabupaten Karawang, Jawa Barat 41374, Indonesia</h3>
                </div>

            </div>
            <div class="col-sm-2">
                <div class="container">
                    <img class="img-fluid" src="logo_bsi.png" alt="Chania">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                    <a class="navbar-brand" href="#">SIA</a>

                </nav>
            </div>
        </div>

        <div class="row full-height">
            <div class="col-sm-3 bg-light">

                <nav class="navbar bg-light">

                    <!-- Links -->
                    <ul class="navbar-nav">
                        <div class="container">
                            <h4>Master Data</h4>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_akun">Akun</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_saldo_awal">Saldo Awal</a></li>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_user">User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_periode">Periode Akuntansi</a>
                            </li>

                            <h4>Transaksi</h4>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_kas_masuk">Kas Masuk</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=data_kas_keluar">Kas Keluar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=jurnal_umum">Jurnal Umum</a>
                            </li>
                            

                            <h4>Informasi Data</h4>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_akun">Master Akun</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_saldo_awal">Saldo Awal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_kas_masuk">Data Kas Masuk</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_kas_keluar">Data Kas Keluar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_user">Data User</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=informasi_jurnal">Data Jurnal</a>
                            </li>
                            <h4>Riwayat Hidup</h4>
                            <li class="nav-item">
                                <a class="nav-link" href="index.jsp?halaman=riwayat_hidup">Riwayat HIdup</a>
                            </li>
                            
                            
                        </div>

                    </ul>
                    
                    

                </nav>

            </div>

            <div class="col-sm-9">
                <c:choose>
                    <c:when test="${param.halaman=='data_akun'}">
                        <%@include file="tampil_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='add_akun'}">
                        <%@include file="add_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_akun'}">
                        <%@include file="edit_akun.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='data_kas_masuk'}">
                        <%@include file="Kas_Masuk.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='data_kas_keluar'}">
                        <%@include file="Kas_Keluar.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='data_user'}">
                        <%@include file="tampil_user.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='add_user'}">
                        <%@include file="add_user.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_user'}">
                        <%@include file="edit_user.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='data_periode'}">
                        <%@include file="tampil_periode.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='edit_periode'}">
                        <%@include file="edit_periode.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='add_periode'}">
                        <%@include file="add_periode.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='data_saldo_awal'}">
                        <%@include file="saldo_awal.jsp" %>
                    </c:when>
                  


                    <c:when test="${param.halaman=='jurnal_umum'}">
                        <%@include file="jurnal_umum.jsp" %>
                    </c:when>


                    <c:when test="${param.halaman=='informasi_user'}">
                        <%@include file="info_user.jsp" %>
                    </c:when>
                    <c:when test="${param.halaman=='informasi_saldo_awal'}">
                        <%@include file="info_saldo_awal.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_kas_masuk'}">
                        <%@include file="info_kas_masuk.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_kas_masuk_detail'}">
                        <%@include file="info_kas_masuk_detail.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_kas_keluar'}">
                        <%@include file="info_kas_keluar.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_kas_keluar_detail'}">
                        <%@include file="info_kas_keluar_detail.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_jurnal'}">
                        <%@include file="info_jurnal.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_jurnal_detail'}">
                        <%@include file="info_jurnal_detail.jsp" %>
                    </c:when>

                    <c:when test="${param.halaman=='informasi_akun'}">
                        <%@include file="info_akun.jsp" %>
                    </c:when>
                    
                    <c:when test="${param.halaman=='riwayat_hidup'}">
                        <%@include file="riwayat_hidup.jsp" %>
                    </c:when>

                    <c:otherwise>
                        <%@include file="home.jsp" %>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>









        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <script src="config/setting.js" ></script>

        <script>

            $(document).ready(function () {
                $('#tabel').DataTable();
            });
        </script>


    </body>
</html>
