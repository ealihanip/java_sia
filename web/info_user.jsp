<%-- 
    Document   : tampil_user
    Created on : Apr 20, 2018, 9:30:20 PM
    Author     : Nindi Musliyanti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %> 
<!DOCTYPE html>

<center><h1>Info Data User</h1></center>
<br>


<table class="table table-bordered" id="tabel">
    <thead>
        <tr>
            <th> User name </th>
            <th> Nama User </th>
            <th> Password </th>
            <th> Hak Akses </th>
            
        </tr>
    </thead>

    <tbody>
         
            <%
                Connection koneksi = null;
                Statement stmt = null;
                ResultSet rs = null;

                Class.forName("com.mysql.jdbc.Driver");
                koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_nindi", "root", "");
                stmt = koneksi.createStatement();
                rs = stmt.executeQuery("SELECT * FROM user");

                int isi = rs.getRow();
                //  out.println(isi);
                if (isi > 0) {
                    out.println("<tr>"
                            + "<td colspan=5>"
                            + " Data Kosong -"
                            + "</td>"
                            + "</tr>");
                } else {
                    while (rs.next()) {
                        out.println("<tr>"
                                + "<td>" + rs.getString(1) + "</td>"
                                + "<td>" + rs.getString(2) + "</td>"
                                + "<td>" + rs.getString(3) + "</td>"
                                + "<td>" + rs.getString(4) + "</td>"
                                + "</tr>");
                    }
                }
            %>
            
    </tbody>
</table>


