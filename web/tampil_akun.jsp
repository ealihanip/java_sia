<%@page import="java.sql.*" %>

<%
    //--koneksi database--
    Connection koneksi = null;
    Statement stmt = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/sia_nindi",
                    "root", "");
    stmt = koneksi.createStatement();
    rs = stmt.executeQuery("SELECT * FROM master_akun");

%>

        <h1>Daftar Rekening [Akun]</h1>

        <br>
        <a href="index.jsp?halaman=add_akun">Tambah Akun</a>
        <br>
        <br>
        <table border="1" class="table table-bordered">
            <tr>
                <th>Kode*</th>
                <th>Nama Akun</th>
                <th>Jenis Akun</th>
                <th>Saldo Normal</th>
                <th>Action</th>
            </tr>
            <%while (rs.next()) {

                    out.println("<tr>"
                            + "<td>" + rs.getString("kode_akun") + "</td>"
                            + "<td>" + rs.getString("nama_akun") + "</td>"
                            + "<td>" + rs.getString("jenis_akun") + "</td>"
                            + "<td>" + rs.getString("saldo_normal") + "</td>"
                            + "<td><a href=index.jsp?halaman=edit_akun&kode="
                            + rs.getString("kode_akun") + ">Edit</a> | "
                            + "<a href=akunServlet?aksi=Delete&kode="
                            + rs.getString("kode_akun") + ">Hapus</a></td>"
                            + "</tr>");
                }

            %>
        </table>

     