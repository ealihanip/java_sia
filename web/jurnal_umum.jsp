<%-- 
    Document   : Jurnal_umum
    Created on : Jun 2, 2018, 12:13:24 AM
    Author     : kanep
--%>

<%@page import="java.sql.*, model.Akun,model.KasMasuk"%>

<%
    KasMasuk kasmasuk = new KasMasuk();
    Akun akun = new Akun();
    Connection koneksi = null;
    Statement stmt = null;

    ResultSet rs = null;
    ResultSet qryakun = null;
    ResultSet qrydeb = null;

    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_nindi", "root", "");
    stmt = koneksi.createStatement();
%>


<h1>Jurnal Umum</h1>

<form action="JurnalServlet" method="post">
    <table border="0">
        <tr>
            <td>
                No Referensi
            </td>
            <td>
                :
            </td>
            <td>

                <%
                    try {
                        ResultSet nor = null;
                        Statement perintah = koneksi.createStatement();
                        nor = perintah.executeQuery("Select max(right(no_referensi,1)) as no from jurnal");

                        while (nor.next()) {

                            if (nor.first() == false) {
                                out.println("<input type='text' name='no_referensi' value='JU00000001'>");

                            } else {

                                nor.last();
                                int autonor = nor.getInt(1) + 1;
                                String nomorr = String.valueOf(autonor);
                                int nolong = nomorr.length();

                                for (int a = 1; a < 9 - nolong; a++) {

                                    nomorr = "0" + nomorr;

                                }

                                String nomerr = "JU" + nomorr;

                                out.println("<input type='text' name='no_referensi' value='" + nomerr + "'>");

                            }
                        }
                    } catch (Exception e) {
                        out.println(e);
                    }


                %>


            </td>

        </tr>
        <tr>

            <td>
                Tanggal Transaksi
            </td>


            <td>
                :
            </td>

            <td>
                <input type='date' name='tgl_trans'>
            </td>
        </tr>
        <tr>

            <td>
                No Bukti Transaksi
            </td>


            <td>
                :
            </td>

            <td>
                <input type='text' name='no_bukti_transaksi'>
            </td>
        </tr>
        <tr>

            <td>
                Keterangan
            </td>


            <td>
                :
            </td>

            <td>
                <textarea name='keterangan' rows="5" cols="30"></textarea>
            </td>


        </tr>

    </table>

    <br>
    <table class="table table-bordered">

        <tr>
            <th>
            </th>
            <th>
                Kode Dan Nama Akun
            </th>
            <th>
                Debet 
            </th>
            <th>
                Kredit
            </th>
        </tr>

        <tr>
            <td align="center">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="pilih[]" value="1">
                    </label>
                </div>

            </td>
            <td>
                <select class="form-control" name="no_akunkredit">


                    <%                        ResultSet no = null;
                        Statement kode1 = koneksi.createStatement();
                        no = kode1.executeQuery("select * from master_akun");
                        while (no.next()) {

                            akun.setKode_akun(no.getString("kode_akun"));
                            akun.setNama_akun(no.getString("nama_akun"));


                    %>

                    <option  value="<%=akun.getKode_akun()%>"><%=akun.getKode_akun()%> <%=akun.getNama_akun()%></option>
                    <%}%>

                </select>


            </td>


            <td>
                <div class="form-group">

                    <input type="text" name="debet" id="debet1" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="kredit" id="kredit1" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="pilih[]" value="2">
                    </label>
                </div>

            </td>
            <td>
                <select class="form-control" name="no_akunkredit">


                    <%

                        Statement kode2 = koneksi.createStatement();
                        no = kode2.executeQuery("select * from master_akun");
                        while (no.next()) {

                            akun.setKode_akun(no.getString("kode_akun"));
                            akun.setNama_akun(no.getString("nama_akun"));


                    %>

                    <option  value="<%=akun.getKode_akun()%>"><%=akun.getKode_akun()%> <%=akun.getNama_akun()%></option>
                    <%}%>

                </select>


            </td>


            <td>
                <div class="form-group">

                    <input type="text" name="debet" id="debet2" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="kredit" id="kredit2" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="pilih[]" value="3">
                    </label>
                </div>

            </td>
            <td>
                <select class="form-control" name="no_akunkredit">


                    <%

                        Statement kode3 = koneksi.createStatement();
                        no = kode3.executeQuery("select * from master_akun");
                        while (no.next()) {

                            akun.setKode_akun(no.getString("kode_akun"));
                            akun.setNama_akun(no.getString("nama_akun"));


                    %>

                    <option  value="<%=akun.getKode_akun()%>"><%=akun.getKode_akun()%> <%=akun.getNama_akun()%></option>
                    <%}%>

                </select>


            </td>


            <td>
                <div class="form-group">

                    <input type="text" name="debet" id="debet3" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="kredit" id="kredit3" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="pilih[]" value="4">
                    </label>
                </div>

            </td>
            <td>
                <select class="form-control" name="no_akunkredit">


                    <%

                        Statement kode4 = koneksi.createStatement();
                        no = kode4.executeQuery("select * from master_akun");
                        while (no.next()) {

                            akun.setKode_akun(no.getString("kode_akun"));
                            akun.setNama_akun(no.getString("nama_akun"));


                    %>

                    <option  value="<%=akun.getKode_akun()%>"><%=akun.getKode_akun()%> <%=akun.getNama_akun()%></option>
                    <%}%>

                </select>


            </td>


            <td>
                <div class="form-group">

                    <input type="text" name="debet" id="debet4"  onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="kredit" id="kredit4" onchange="Hitung()" class="form-control" value="0">

                </div>
            </td>
        </tr>

        <tr>

            <td colspan="2" style="text-align:center;">
                Total
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="TtlDebet" id="TtlDebet" onchange="Hitung()" class="form-control">

                </div>
            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="TtlKredit" id="TtlKredit" onchange="Hitung()" class="form-control">

                </div>
            </td>
        </tr>

        <tr>

            <td colspan="2" style="text-align:center;">
                Balance
            </td>
            <td>

            </td>
            <td>
                <div class="form-group">

                    <input type="text" name="Balance" id="Balance" class="form-control">

                </div>
            </td>

        </tr>
        <tr>
            <td colspan="4" align="center">
                <button type="submit" class="btn btn-primary" name="aksi" value="simpan">Simpan</button>
            <td>
        </tr>

    </table>
</form>


