<%-- 
    Document   : edit_periode
    Created on : Apr 2, 2018, 6:49:47 PM
    Author     : Nindi Musliyanti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*, model.periode"%>
<%
    periode periode = new periode();
    Connection koneksi = null;
    Statement query = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_Nindi", "root", "");
    query = koneksi.createStatement();
    String tahun = request.getParameter("thn");
    if (tahun != null) {
        rs = query.executeQuery("SELECT * FROM periode "
                + "WHERE thn = '" + tahun + "'");
        if (rs.next()) {
            periode.setThn(rs.getString("thn"));
            periode.setAwal_bulan(rs.getString("awal_bulan"));
            periode.setAkhir_bln(rs.getString("akhir_bulan"));
            periode.setStatus(rs.getString("status"));

        }
    }
%>
<h1>Edit Data Periode</h1>
<form action="PeriodeServlet" method="POST">
    <table border="0" cellpadding="4">
        <tr>
            <td>tahun</td>
            <td>:</td>
            <td><input type="text" name="tahun" size="10"
                       value="<%=periode.getThn()%>" readonly=""/></td>
        </tr>
        <tr>
            <td>awal bulan periode</td>
            <td>:</td>
            <td><input type="text" name="awal_bulan" size="20"
                       value="<%=periode.getAwal_bulan()%>"/></td>
        </tr>
        <tr>
            <td>akhir bulan periode</td>
            <td>:</td>
            <td><input type="text" name="akhir_bulan" size="20"
                       value="<%=periode.getAkhir_bln()%>"/></td>
        </tr>
        <tr>
            <td>status</td>
            <td>:</td>
            <td>
            <%
                if (periode.getStatus().equalsIgnoreCase("AKTIF")) {%>
            <input type="radio" name="status" value="AKTIF" checked="checked"/>AKTIF
                <input type="radio" name="status" value="NONAKTIF"/>NONAKTIF
                <% } else { %>
                <input type="radio" name="status" value="AKTIF"/>AKTIF
                <input type="radio" name="status" value="NONAKTIF" checked="checked"/>NONAKTIF
                <% }%>
            </td>
        </tr>
        <br>
        <tr>
            <td colspan="2">
                <input type="submit" value="Update" name="aksi"/>
            </td>
        </tr>
    </table>
</form>
