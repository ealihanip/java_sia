<%-- 
    Document   : Kas_Keluar
    Created on : Apr 15, 2018, 11:07:30 AM
    Author     : Nindi Musliyanti
--%>

<%@page import="java.sql.*, model.Akun,model.KasKeluar"%>
<%
    KasKeluar kaskeluar = new KasKeluar();
    Akun akun = new Akun();
    Connection koneksi = null;
    Statement stmt = null;

    ResultSet rs = null;
    ResultSet qryakun = null;
    ResultSet qrydeb = null;

    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_nindi", "root", "");
    stmt = koneksi.createStatement();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css"/>
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Kas Keluar</h1>
        <form action="KasKeluarServlet" method="post">

            <table border="0">
                <tr>
                    <td>
                        No. Kas Keluar
                    </td>
                    <td>
                        :
                    </td>
                    <td>

                        <%
                            try {
                                ResultSet nokk = null;
                                Statement perintah = koneksi.createStatement();
                                nokk = perintah.executeQuery("Select max(right(no_kk,1)) as no from kas_out");

                                while (nokk.next()) {

                                    if (nokk.first() == false) {
                                        out.println("<input type='text' name='no_kk' value='KK00000001'>");

                                    } else {

                                        nokk.last();
                                        int autonokm = nokk.getInt(1) + 1;
                                        String nomorkk = String.valueOf(autonokm);
                                        int nolong = nomorkk.length();

                                        for (int a = 1; a < 9 - nolong; a++) {

                                            nomorkk = "0" + nomorkk;

                                        }

                                        String nomerkk = "KK" + nomorkk;

                                        out.println("<input type='text' name='no_kk' value='" + nomerkk + "'>");

                                    }
                                }
                            } catch (Exception e) {
                                out.println(e);
                            }

                        %>


                    </td>

                </tr>
                <tr>
                    <td>

                        Akun Debet
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <select name='no_akundebet'>

                            <%qrydeb = stmt.executeQuery("select kode_akun,nama_akun from master_akun where left(kode_akun,2)=11");
                                while (qrydeb.next()) {

                                    akun.setKode_akun(qrydeb.getString("kode_akun"));
                                    akun.setNama_akun(qrydeb.getString("Nama_akun"));


                            %>

                            <option
                                value="<%=akun.getKode_akun()%>"    

                                >

                                <%=akun.getKode_akun()%> <%=akun.getNama_akun()%>



                            </option>

                            <%}%>
                        </select>
                    </td>

                </tr>

                <tr>

                    <td>
                        Tanggal
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <input type='date' name='tgl_kk'>
                    </td>
                </tr>
                <tr>

                    <td>
                        memo
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <textarea name='memo' rows="5" cols="30"></textarea>
                    </td>


                </tr>
                <tr>

                    <td>
                        Akun Kas
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <select name='no_akunkredit'>
                            <% qryakun = stmt.executeQuery("select kode_akun,nama_akun from master_akun");
                                while (qryakun.next()) {

                                    akun.setKode_akun(qryakun.getString("kode_akun"));
                                    akun.setNama_akun(qryakun.getString("Nama_akun"));


                            %>

                            <option
                                value="<%=akun.getKode_akun()%>"    

                                >

                                <%=akun.getKode_akun()%> <%=akun.getNama_akun()%>



                            </option>

                            <%}%>
                        </select>
                    </td>
                </tr>

                <tr>

                    <td>
                        Nominal
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <input type='text' name='nominal'>
                    </td>
                </tr>
                <tr>

                    <td colspan="3">
                        <input type='submit' name='aksi' value='SIMPAN'>
                    </td>
                </tr>
            </table>
        </form>

        <table class="table table-bordered">

            <tr>
                <th>
                    No Kas Keluar
                </th>

                <th>
                    Tanggal Kas Keluar
                </th>

                <th>
                    Memo
                </th>

                <th>
                    Akun
                </th>
                <th>
                    Nominal
                </th>

                <th>
                    Action
                </th>
            </tr>



            <%
                rs = stmt.executeQuery("SELECT kas_out.no_kk,kas_out.tgl_kk,kas_out.memo,kas_out_detail.kode_akun,kas_out_detail.nominal "
                        + "FROM kas_out "
                        + "INNER JOIN kas_out_detail "
                        + "ON kas_out_detail.no_kk=kas_out.no_kk");

                while (rs.next()) {
                    out.println("<tr>");

                    out.println("<td>" + rs.getString(1) + "</td>");
                    out.println("<td>" + rs.getString(2) + "</td>");
                    out.println("<td>" + rs.getString(3) + "</td>");
                    out.println("<td>" + rs.getString(4) + "</td>");
                    out.println("<td>" + rs.getString(5) + "</td>");
                    out.println("<td>" + "<a href=KasKeluarServlet?aksi=HAPUS&no_kk=" + rs.getString(1) + ">Hapus</a>" + "</td>");
                    out.println("</tr>");

                }


            %>




        </table>




    </body>
</html>
