<%-- 
    Document   : Kas_Masuk
    Created on : Apr 15, 2018, 11:07:30 AM
    Author     : Nindi Musliyanti
--%>

<%@page import="java.sql.*, model.Akun,model.KasMasuk"%>
<%
    KasMasuk kasmasuk = new KasMasuk();
    Akun akun = new Akun();
    Connection koneksi = null;
    Statement stmt = null;

    ResultSet rs = null;
    ResultSet qryakun = null;
    ResultSet qrydeb = null;

    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_nindi", "root", "");
    stmt = koneksi.createStatement();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css"/>
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Kas Masuk</h1>
        <form action="KasMasukServlet" method="post">

            <table border="0">
                <tr>
                    <td>
                        No. Kas Masuk
                    </td>
                    <td>
                        :
                    </td>
                    <td>

                        <%
                            try {
                                ResultSet nokm = null;
                                Statement perintah = koneksi.createStatement();
                                nokm = perintah.executeQuery("Select max(right(no_km,1)) as no from kas_in");

                                while (nokm.next()) {

                                    if (nokm.first()==false) {
                                        out.println("<input type='text' name='no_km' value='KM00000001'>");

                                    } else {

                                        nokm.last();
                                        int autonokm = nokm.getInt(1) + 1;
                                        String nomorkm = String.valueOf(autonokm);
                                        int nolong = nomorkm.length();

                                        for (int a = 1; a < 9 - nolong; a++) {

                                            nomorkm = "0" + nomorkm;

                                        }

                                        String nomerkm = "KM" + nomorkm;

                                        out.println("<input type='text' name='no_km' value='" + nomerkm + "'>");

                                    }
                                }
                            } catch (Exception e) {
                                out.println(e);
                            }

                        %>


                    </td>

                </tr>
                <tr>
                    <td>

                        Akun Debet
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <select name='no_akundebet'>
                            <%                            qrydeb = stmt.executeQuery("select kode_akun,nama_akun from master_akun where left(kode_akun,2)=11");
                                while (qrydeb.next()) {

                                    akun.setKode_akun(qrydeb.getString("kode_akun"));
                                    akun.setNama_akun(qrydeb.getString("Nama_akun"));

                                
                            %>

                            <option
                                value="<%=akun.getKode_akun()%>"    

                                >

                                <%=akun.getKode_akun()%> <%=akun.getNama_akun()%>



                            </option>
                            
                            <%}%>
                        </select>
                    </td>

                </tr>

                <tr>

                    <td>
                        Tanggal
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <input type='date' name='tgl_km'>
                    </td>
                </tr>
                <tr>

                    <td>
                        Keterangan
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <textarea name='keterangan' rows="5" cols="30"></textarea>
                    </td>


                </tr>
                <tr>

                    <td>
                        Akun Kas
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <select name='no_akunkredit'>
                            <% qryakun = stmt.executeQuery("select kode_akun,nama_akun from master_akun");
                                while (qryakun.next()) {

                                    akun.setKode_akun(qryakun.getString("kode_akun"));
                                    akun.setNama_akun(qryakun.getString("Nama_akun"));

                                
                            %>

                            <option
                                value="<%=akun.getKode_akun()%>"    

                                >

                                <%=akun.getKode_akun()%> <%=akun.getNama_akun()%>



                            </option>
                            
                            <%}%>
                        </select>
                    </td>
                </tr>

                <tr>

                    <td>
                        Nominal
                    </td>


                    <td>
                        :
                    </td>

                    <td>
                        <input type='text' name='nominal'>
                    </td>
                </tr>
                <tr>

                    <td colspan="3">
                        <input type='submit' name='aksi' value='SIMPAN'>
                    </td>
                </tr>
            </table>
        </form>

        <table class="table table-bordered">

            <tr>
                <th>
                    No Kas Masuk
                </th>

                <th>
                    Tanggal Kas Masuk
                </th>

                <th>
                    keterangan
                </th>

                <th>
                    Akun
                </th>
                <th>
                    Nominal
                </th>

                <th>
                    Action
                </th>
            </tr>

            

                    <%
                        rs = stmt.executeQuery("SELECT kas_in.no_km,kas_in.tgl_km,kas_in.keterangan,kas_in_detail.kode_akun,kas_in_detail.nominal "
                                + "FROM kas_in "
                                + "INNER JOIN kas_in_detail "
                                + "ON kas_in_detail.no_km=kas_in.no_km");

                        while (rs.next()) {
                            out.println("<tr>");
                            
                            out.println("<td>"+ rs.getString(1) +"</td>");
                            out.println("<td>"+ rs.getString(2) +"</td>");
                            out.println("<td>"+ rs.getString(3) +"</td>");
                            out.println("<td>"+ rs.getString(4) +"</td>");
                            out.println("<td>"+ rs.getString(5) +"</td>");
                            out.println("<td>"+ "<a href=KasMasukServlet?aksi=HAPUS&no_km="+ rs.getString(1)+">Hapus</a>" +"</td>");
                            out.println("</tr>");
                            
                        }


                    %>

                


        </table>




    </body>
</html>
