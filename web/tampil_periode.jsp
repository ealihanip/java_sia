<%-- 
    Document   : tampil_periode
    Created on : Mar 19, 2018, 7:23:53 PM
    Author     : Nindi Musliyanti
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>


<h1>Data Tahun Periode</h1>
<table class="table table-bordered">
    <a href="index.jsp?halaman=add_periode"> Tambah Periode</a>
    <tr>
        <td>Tahun</td>
        <td>Awal Tahun</td>
        <td>Akhir Tahun</td>
        <td>Status</td>
        <td>Action</td>
    </tr>
    <tr>
        <%
            Connection koneksi = null;
            Statement stmt = null;
            ResultSet rs = null;

            Class.forName("com.mysql.jdbc.Driver");
            koneksi = DriverManager.getConnection("jdbc:mysql://localhost/sia_nindi", "root", "");
            stmt = koneksi.createStatement();
            rs = stmt.executeQuery("SELECT * FROM periode ORDER BY thn");

            int isi = rs.getRow();
            if (isi > 0) {
                out.println("<tr class=isi>"
                        + "<td colspan=5> "
                        + "- Data Periode Kosong -"
                        + "</td>"
                        + "</tr>");
            } else {
                while (rs.next()) {
                    out.println("<tr class=isi>"
                            + "<td>" + rs.getString(1) + "</td>"
                            + "<td>" + rs.getString(2) + "</td>"
                            + "<td>" + rs.getString(3) + "</td>"
                            + "<td>" + rs.getString(4) + "</td>"
                            + "<td> <a href=index.jsp?halaman=edit_periode&thn=" + rs.getString("thn") + ">Edit</a>"
                            + "<a href=PeriodeServlet?aksi=Delete&tahun=" + rs.getString("thn") + "> Hapus</a></td>"
                            + "</tr>");

                }
            }
        %>
    </tr>
</table>
